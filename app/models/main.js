// //import hàm cộng tổng hai số
const sum = require('../controllers/myBigNumber.js');
//import file logging.js
const logging = require("../controllers/logging");
const { loggers } = require("winston");

//In ra kết quả sử dụng logger lưu lại lịch sử
logging.info(`Tổng hai số lớn: ${sum("190", "4856")}`);
