//Import thư viện winston
const winston = require('winston');
//Khai báo sử dụng winston
const logging = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.prettyPrint(),
    ),
    transports: [new winston.transports.Console()]
});

module.exports = logging;