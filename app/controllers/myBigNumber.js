//Khai báo hàm cộng hai số
function sum(stn1, stn2) {
    let i = stn1.length - 1;
    let j = stn2.length - 1;

    let tong = [];
    let tam = 0;

    while (i >= 0 || j >= 0) {
        let n1 = stn1[i] || 0;
        let n2 = stn2[j] || 0;
        let tTong = parseInt(n1) + parseInt(n2) + tam;

        let nho = tTong % 10;
        tong.push(nho);
        tam = tTong >= 10 ? 1 : 0;

        j--;
        i--;
    }
    return tong.reverse().join("");
}

module.exports = sum;